// count total balance
let currencyRate = {
    eth: 540.06,
    XETH: 540.06,
    XLTC: 160.84,
    ZUSD: 1,
    ZEUR: 1.23,
    alg: 1
}

function getTotalEthAlg() {
    if (window.localStorage.ethwallets) {
        var wallets = JSON.parse(localStorage.getItem('ethwallets'));
        var keys = Object.keys(wallets);
        var totalEthArr = [];
        var totalAlgArr = [];
        for (var wallet of keys) {
            var addr = '0x' + wallet;
            totalAlgArr.push(web3.toDecimal(web3.toHex(instance.balanceOf(addr))));
            totalEthArr.push(web3.toDecimal(web3.toHex(web3.fromWei(web3.eth.getBalance(addr)))));
        }
        let totalEth = totalEthArr.reduce(function (a, b) {
            a + b
        });
        let totalAlg = totalAlgArr.reduce(function (a, b) {
            a + b
        });

        let ethUsd = totalEth * currencyRate.eth;
        let algUsd = totalAlg * currencyRate.alg;
        let totalUsd = ethUsd + algUsd;

        // save to LS
        localStorage.setItem('totaleth', JSON.stringify(ethUsd));
        localStorage.setItem('totalalg', JSON.stringify(algUsd));
        localStorage.setItem('totalbtc', JSON.stringify(0));

        return totalUsd.toFixed(2);
    }

    localStorage.setItem('totaleth', JSON.stringify(0));
    localStorage.setItem('totalalg', JSON.stringify(0));
    localStorage.setItem('totalbtc', JSON.stringify(0));

    return 0;
}

function makeKrakenOrder(signal) {
    if (signal.action.strategy_name === 'portfolioM') {
        fetch('http://remount.me/kraken/get_account_balance?id=11067798')
            .then(res => res.json())
            .then(res => {
                let result = res.result;
                // crop key name
                let oldKey = Object.keys(result)[0];
                let newKey = oldKey.slice(1, oldKey.length);
                let val;
                if (oldKey !== newKey) {
                    Object.defineProperty(result, newKey, Object.getOwnPropertyDescriptor(result, oldKey));
                    delete result[oldKey];
                }
                console.log(signal);

                switch (signal.action.instrument) {
                    case 'XETH-ZUSD':
                        console.log(signal);
                        val = Number(result.XETH) / 100 * parseInt(signal.action.quantity) - Number(result.XETH) / 100
                        val = val.toFixed(4);

                        // add order
                        if (signal.action.action === 'exchange') {
                            let args = {
                                pair: 'XETHZUSD',
                                type: 'sell',
                                ordertype: 'market',
                                volume: val
                            }

                            $.ajax({
                                type: 'post',
                                url: 'http://remount.me/kraken/add_order?id=11067798',
                                data: JSON.stringify(args),
                                success: (res) => {
                                    let t = res.match(/"E.*?/);
                                    if (t) alert('Error!');
                                    console.log(res, 'kraken add order res')
                                }
                            })
                        }
                        break;
                    case 'ZUSD-XETH':
                    console.log('inCHres',result);
                        val = Number(result.USD) / 100 * parseInt(signal.action.quantity) - Number(result.USD) / 100;
                        val = val/(currencyRate.eth+currencyRate.eth/100*10);
                        val = val.toFixed(4);
                        console.log('val: ', val);
                        
                        // add order
                        if (signal.action.action === 'exchange') {
                            let args = {
                                pair: 'XETHZUSD',
                                type: 'buy',
                                ordertype: 'market',
                                volume: val
                            }

                            $.ajax({
                                type: 'post',
                                url: 'http://remount.me/kraken/add_order?id=11067798',
                                data: JSON.stringify(args),
                                success: (res) => {
                                    let t = res.match(/"E.*?/);
                                    if (t) alert('Error!');
                                    console.log(res, 'kraken add order res')
                                }
                            })
                        }
                        break;
                    default:
                        console.log(signal.action.instrument)
                        alert('Pair not found');
                }
            })
    }

}
