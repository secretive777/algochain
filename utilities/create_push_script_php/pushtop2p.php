<?php
$subscriberFile = fopen("subscriber.txt", "r") or die("Unable to open file!");
$subscriber = fgets($subscriberFile);
fclose($subscriberFile);

$pushTemplateFile = fopen("push-template.json", "r") or die("Unable to open file!");

$pushTemplate = fread($pushTemplateFile, filesize("push-template.json"));

$search = ['{{$subscriber}}', '{{$action}}','{{$quantity}}'];
$replace = [$subscriber, (rand(0, 999) > 500 ? 'buy' : 'sell'),(rand(1,5))/1000];
$pushMessage = str_replace($search, $replace, $pushTemplate);

$params = array(
    'message' => $pushMessage
);
$url = 'http://18.218.186.169:9970/Send_message';
$ch = curl_init($url);
$postString = http_build_query($params, '', '&');

curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$response = curl_exec($ch);
curl_close($ch);
?>
