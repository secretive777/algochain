const express = require('express');
const app = express();
const Web3 = require('web3');
const Tx = require('ethereumjs-tx');
const fetch = require('node-fetch');
const bodyParser = require('body-parser');

// web3 data
const web3 = new Web3(new Web3.providers.HttpProvider("http://52.174.19.111:8545"));
const algAbi = JSON.stringify([{ "constant": true, "inputs": [], "name": "name", "outputs": [{ "name": "", "type": "string", "value": "AlgoChain" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_spender", "type": "address" }, { "name": "_value", "type": "uint256" }], "name": "approve", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "totalSupply", "outputs": [{ "name": "", "type": "uint256", "value": "20000000" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_from", "type": "address" }, { "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" }], "name": "transferFrom", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "decimals", "outputs": [{ "name": "", "type": "uint256", "value": "0" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "kill", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "_amount", "type": "uint256" }], "name": "burn", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "name": "_owner", "type": "address" }], "name": "balanceOf", "outputs": [{ "name": "balance", "type": "uint256", "value": "1000000" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "symbol", "outputs": [{ "name": "", "type": "string", "value": "ALG" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_to", "type": "address" }, { "name": "_amount", "type": "uint256" }], "name": "transfer", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "name": "_amount", "type": "uint256", "index": 0, "typeShort": "uint", "bits": "256", "displayName": "&thinsp;<span class=\"punctuation\">_</span>&thinsp;amount", "template": "elements_input_uint", "value": "21000000" }, { "name": "_name", "type": "string", "index": 1, "typeShort": "string", "bits": "", "displayName": "&thinsp;<span class=\"punctuation\">_</span>&thinsp;name", "template": "elements_input_string", "value": "AlgoChain" }, { "name": "_symbol", "type": "string", "index": 2, "typeShort": "string", "bits": "", "displayName": "&thinsp;<span class=\"punctuation\">_</span>&thinsp;symbol", "template": "elements_input_string", "value": "ALG" }, { "name": "_decimals", "type": "uint256", "index": 3, "typeShort": "uint", "bits": "256", "displayName": "&thinsp;<span class=\"punctuation\">_</span>&thinsp;decimals", "template": "elements_input_uint", "value": "0" }], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "payable": true, "stateMutability": "payable", "type": "fallback" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "_deposited", "type": "uint256" }], "name": "EtherDeposit", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "_spender", "type": "address" }, { "indexed": false, "name": "_value", "type": "uint256" }], "name": "Approval", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "_from", "type": "address" }, { "indexed": true, "name": "_to", "type": "address" }, { "indexed": false, "name": "_value", "type": "uint256" }], "name": "Transfer", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "_contract", "type": "address" }, { "indexed": true, "name": "_sentAmount", "type": "uint256" }], "name": "Suicide", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "_burnt", "type": "uint256" }, { "indexed": true, "name": "_totalSupply", "type": "uint256" }], "name": "Burnt", "type": "event" }])
const contractAddr = '0xEF765729e087d0289Df4285CbdbA6940aABbECe5';
const instance = web3.eth.contract(JSON.parse(algAbi)).at(contractAddr);

const owner = '0x3819E5cD09C3A8Ff2bF69667f91BB466C1115c04';
const privateKey = new Buffer('aa43d9aac4ceb89e4d6d98ad0fa318243f2b11a6e48b082280878d1a8591e76e', 'hex');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

let eLoop = [];
let inProgress = false;

// func template
processTx = (data) => {
    inProgress = true;

    console.log('Initiated transaction.')

    // transaction
    const value = 2;
    const to = data.wallet_from;

    web3.eth.getGasPrice(function (err, data) {
        let gasPriceHex = web3.toHex(data);

        let transactionObject = {
            from: owner,
            to: contractAddr,
            gasPrice: gasPriceHex
        }

        let rawData = instance.transfer.getData(to, value, transactionObject);

        web3.eth.estimateGas({
            from: owner,
            to: contractAddr,
            data: rawData
        }, (err, result) => {
            let rawTx = {
                nonce: web3.toHex(web3.eth.getTransactionCount(owner)),
                gasPrice: gasPriceHex,
                gasLimit: web3.toHex(result * 2),
                to: contractAddr,
                data: rawData,
                chainId: 4
            }

            // form & sign transaction
            let tx = new Tx(rawTx);
            tx.sign(privateKey);
            let serializedTx = tx.serialize();

            // send transaction
            web3.eth.sendRawTransaction(`0x${serializedTx.toString('hex')}`, function (err, hash) {
                if (err) { throw err }

                let s, receipt;
                s = setInterval(() => {
                    if (!receipt) {
                        try {
                            receipt = web3.eth.getTransactionReceipt(hash);
                        } catch (err) {
                            console.log('Waiting for receipt...');
                        }
                    } else {
                        clearInterval(s);
                        console.log('Transaction successful!');

                        if (web3.toDecimal(receipt.status) == 1) {
                            if (eLoop.length > 0) {
                                setTimeout(() => {
                                    eLoop.shift().call()
                                }, 10000)
                            } else {
                                inProgress = false;
                                console.log('Event loop is empty.')
                            }
                        }
                    }
                }, 10000)
            });
        });
    });

}

setInterval(() => {

    fetch('http://maxdata.webmakom.com/storage/?method=get_data&collection=algoexchangesinfo&handled=false')
        .then(res => res.json())
        .then(res => {
            let dataArr = res.result;

            for (let record of dataArr) {
                if (record.status == '1') {
                    fetch(`http://maxdata.webmakom.com/storage/?method=update_data&collection=algoexchangesinfo&internal_id=${record.internal_id}&handled=true`)
                    eLoop.push(processTx.bind(this, record));
                }
            }

            !inProgress && eLoop.length > 0 ? eLoop.shift().call() : '';
        })

}, 60000)


app.listen(4345, console.log('listening to :4345...'));