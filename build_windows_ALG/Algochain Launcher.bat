@echo off
title Algochain Launcher

echo Starting SaiCryptoModule
cd build-SaiCryptoModule-Windows
start SaiCryptoModule.exe

cd ..

echo Starting SaiView
cd build-SaiView_Windows_resizeble/release
start SaiView.exe --remote-debugging-port=4567
cd ..
cd ..
cd p2p_win32
timeout /t 5 
start p2p_win32.exe
cd ..

